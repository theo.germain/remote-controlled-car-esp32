#include "wifi_conf.hh"

#ifdef AP_MODE
#define SSID "Remote Controlled Car"
#define PASSWORD "123456789"
#else
#define SSID "Freebox-1C9D3E"
#define PASSWORD "phalino4@-jussisse7.-fornicem-perimendas"
#endif


void initWiFi()
{
    Serial.printf("wifi ssid = %s\n", SSID);
    Serial.printf("wifi password = %s\n", PASSWORD);

    #ifdef AP_MODE
    tcpip_adapter_ip_info_t ipInfo;
    ESP_ERROR_CHECK(nvs_flash_init());
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(NULL, NULL));

    wifi_init_config_t wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_init_config));
    
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));

    wifi_config_t wifi_config = {};
    strcpy((char*)wifi_config.ap.ssid, SSID);
    strcpy((char*)wifi_config.ap.password, PASSWORD);
    wifi_config.ap.authmode = WIFI_AUTH_WPA2_PSK;
    wifi_config.ap.max_connection = 4;

    esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config);
    tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_AP, &ipInfo);

    ESP_ERROR_CHECK(esp_wifi_start());

    // IP address.
    Serial.print("My IP: ");
    Serial.printf(IPSTR, IP2STR(&ipInfo.ip));
    Serial.println("");

    #else
    WiFi.disconnect(true);
    WiFi.mode(WIFI_STA);
    WiFi.begin(SSID, PASSWORD);
 
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println(WiFi.localIP());
    #endif
}
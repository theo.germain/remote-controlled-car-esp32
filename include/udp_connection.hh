#pragma once

#include "main.hh"
#include "json_decoder.hh"
#include "config.h"

#define LISTENING_PORT 9999

extern IPAddress CLI_IP_ADDR;
extern int WRITING_PORT;

void initConnection();
void sendUdpData(uint8_t *buff, size_t len);
#pragma once

#include <Arduino.h>

#define LED_PIN 4

void initLed();
void turnOnLed();
void turnOffLed();